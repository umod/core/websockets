﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading;
using uMod.Common.WebSockets;
using Websocket.Client;

namespace uMod.Tests
{
    [TestClass]
    public class RemoteServerTest : MockClient
    {
        [ClassInitialize]
        public static void Listen(TestContext context)
        {
            CreateClient();
        }

        [ClassCleanup]
        public static void Stop()
        {
            CloseClient();
        }

        private readonly ConnectionInfo ValidConnection = new ConnectionInfo()
        {
            Location = "127.0.0.1",
            Port = 8080,
            Protocol = "ws",
            Name = "test"
        };

        public Packet CreatePacket(string message)
        {
            return new Packet()
            {
                Message = message,
                Type = "test",
            };
        }

        [TestMethod]
        public void ConnectSuccess()
        {
            EnqueueMessage(CreateMessage(WebSocketMessageType.ServerStart, ValidConnection));

            Assert.AreEqual(1, lastMessages.Count);
            Assert.AreEqual(ConnectionState.Open, lastMessages[0].Data);

            EnqueueMessage(CreateMessage(WebSocketMessageType.ServerClose, "test"));

            Assert.AreEqual(1, lastMessages.Count);
            Assert.AreEqual(ConnectionState.Closed, lastMessages[0].Data);
        }

        [TestMethod]
        public void PacketSendReceive()
        {
            EnqueueMessage(CreateMessage(WebSocketMessageType.ServerStart, ValidConnection));

            Assert.AreEqual(1, lastMessages.Count);
            Assert.AreEqual(ConnectionState.Open, lastMessages[0].Data);

            int msgCount = 0;
            WebSocketMessage lastMessage = null;
            int retryCount = 0;
            var url = new Uri($"{ValidConnection.Protocol}://{ValidConnection.Location}:{ValidConnection.Port}");
            WebsocketClient client = null;
            using (client = new WebsocketClient(url))
            {
                client.ReconnectTimeout = TimeSpan.FromSeconds(30);
                client.MessageReceived.Subscribe(msg => msgCount++);
                client.Start();

                while (!client.IsRunning) // wait until connection is established
                {
                    retryCount++;
                    Thread.Sleep(25);
                    if (retryCount > 50)
                    {
                        break;
                    }
                }
                client.Send(JsonConvert.SerializeObject(CreatePacket("hello")));

                Console.WriteLine("Waiting");
                Thread.Sleep(1000);

                Assert.IsTrue(lastMessages.Count > 0);
                lastMessage = lastMessages.LastOrDefault();
                Console.WriteLine(lastMessage.Type);
                Assert.AreEqual(WebSocketMessageType.SocketPacket, lastMessage.Type);
            }

            retryCount = 0;
            while (client.IsRunning) // wait until connection is established
            {
                retryCount++;
                Thread.Sleep(25);
                if (retryCount > 50)
                {
                    break;
                }
            }

            Thread.Sleep(25);

            lastMessage = lastMessages.LastOrDefault();
            Console.WriteLine(lastMessage.Type);
            Assert.AreEqual(WebSocketMessageType.SocketClose, lastMessage.Type);

            EnqueueMessage(CreateMessage(WebSocketMessageType.ServerClose, "test"));

            Assert.AreEqual(1, lastMessages.Count);
            Assert.AreEqual(ConnectionState.Closed, lastMessages[0].Data);
        }
    }
}
