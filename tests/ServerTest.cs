using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using uMod.Common;
using uMod.Common.WebSockets;
using uMod.WebSockets;
using Websocket.Client;
using Websocket.Client.Logging;

namespace uMod.Tests
{
    [TestClass]
    public class ServerTest
    {
        private Random rnd = new Random();
        private static readonly SocketService service = new SocketService(AppContext.BaseDirectory);
        private static ObjectStreamConnection<WebSocketMessage, WebSocketMessage> conn;
        protected static List<WebSocketMessage> lastMessages = new List<WebSocketMessage>();

        [ClassInitialize]
        public static void Listen(TestContext context)
        {
            lastMessages.Clear();
            service.Server = new ObjectStreamClient<WebSocketMessage, WebSocketMessage>(new MemoryStream(), new MemoryStream());
            service.Server.Start();
        }

        private readonly ConnectionInfo ValidConnection = new ConnectionInfo()
        {
            Location = "127.0.0.1",
            Name = "test",
            Port = 8080,
            Protocol = "ws"
        };

        private readonly ConnectionInfo InvalidConnectionInfo = new ConnectionInfo()
        {
            Location = "127.0.0.1",
            Name = "test",
            Port = -8080,
            Protocol = "ws"
        };

        private WebSocketMessage CreateMessage(WebSocketMessageType messageType, object data, object extraData = null, string clientId = null)
        {
            return new WebSocketMessage()
            {
                Id = rnd.Next(1, 999999),
                Type = messageType,
                Data = data,
                ExtraData = extraData,
                ClientId = clientId
            };
        }

        public Packet CreatePacket(string message)
        {
            return new Packet()
            {
                Message = message,
                Type = "test",
            };
        }

        [TestMethod]
        public void ConnectFail()
        {
            WebSocketMessage response = service.TryHandle(service.HandleServerStart, conn, CreateMessage(WebSocketMessageType.ServerStart, InvalidConnectionInfo));

            Assert.IsTrue(response.Data.ToString().StartsWith("General exception: Invalid URI: Invalid port specified."));
        }

        [TestMethod]
        public void ConnectStatusOpen()
        {
            WebSocketMessage connectionMessage = service.HandleServerStart(
                conn,
                CreateMessage(WebSocketMessageType.ServerStart, ValidConnection)
            );

            WebSocketMessage statusResponse = service.Status(
                conn,
                CreateMessage(WebSocketMessageType.ServerStatus, "test")
            );

            Assert.AreEqual(ConnectionState.Open, statusResponse.Data);
        }

        [TestMethod]
        public void ConnectDisconnectStatus()
        {
            WebSocketMessage response = service.HandleServerStart(
                conn,
                CreateMessage(WebSocketMessageType.ServerStart, ValidConnection)
            );

            Assert.AreEqual(ConnectionState.Open, response.Data);

            service.HandleClose(
                conn,
                CreateMessage(WebSocketMessageType.ServerClose, "test")
            );

            WebSocketMessage statusResponse = service.Status(
                conn,
                CreateMessage(WebSocketMessageType.ServerStatus, "test")
            );

            Assert.AreEqual(ConnectionState.Closed, statusResponse.Data);
        }

        public class MyLogProvider : ILogProvider
        {
            private Logger _logger;

            public Logger GetLogger(string name)
            {
                if (_logger != null) return _logger;
                return _logger = new Logger(MyLogFunction);
            }

            public IDisposable OpenMappedContext(string key, object value, bool destructure = false)
            {
                throw new NotImplementedException();
            }

            public IDisposable OpenNestedContext(string message)
            {
                throw new NotImplementedException();
            }
        }

        public static bool MyLogFunction(Websocket.Client.Logging.LogLevel level, Func<string> func, Exception exception = null, params object[] formatParameters)
        {
            if (func == null) return true;
            string msg = func();
            Console.WriteLine(msg);
            return true;
        }

        [TestMethod]
        public void PacketSendReceive()
        {
            ILogger logger = service.CreateDebugLogger();
            service.HandleServerStart(
                conn,
                CreateMessage(WebSocketMessageType.ServerStart, ValidConnection)
            );

            SocketServer server = service.GetServer("test");
            string helloPacket = JsonConvert.SerializeObject(CreatePacket("hello"));

            string lastMessage = string.Empty;
            int msgCount = 0;
            var url = new Uri($"{ValidConnection.Protocol}://{ValidConnection.Location}:{ValidConnection.Port}");
            LogProvider.SetCurrentLogProvider(new MyLogProvider());
            using (var client = new WebsocketClient(url))
            {
                client.IsReconnectionEnabled = true;
                client.ReconnectTimeout = TimeSpan.FromSeconds(30);
                client.DisconnectionHappened.Subscribe(msg =>
                {
                    Console.WriteLine($"{msg.Type}: {msg.CloseStatus} - {msg.CloseStatusDescription}");
                });
                client.MessageReceived.Subscribe(msg =>
                {
                    // assertion, did the client receive the packet we sent?
                    msgCount++;
                });
                client.Start();
                int retryCount = 0;
                while (!client.IsRunning) // wait until connection is established
                {
                    retryCount++;
                    Thread.Sleep(25);
                    if (retryCount > 50)
                    {
                        break;
                    }
                }

                IWebSocketConnection socket = null;

                var msg = CreateMessage(WebSocketMessageType.SocketPacket, helloPacket, "test");
                while (client.IsRunning)
                {
                    if (server.ConnectionCount > 0)
                    {
                        socket = server.GetConnections().First();
                        msg.ClientId = socket.Id;
                        service.Send(conn, msg); // send a packet to the client above (from the server)
                        break;
                    }
                }

                retryCount = 0;
                while (msgCount == 0)
                {
                    retryCount++;
                    Thread.Sleep(25);
                    if (retryCount > 50)
                    {
                        break;
                    }
                }

                Assert.AreEqual(1, msgCount);
            }

            service.HandleClose(
                conn,
                CreateMessage(WebSocketMessageType.ServerClose, "test")
            );
        }
    }
}
