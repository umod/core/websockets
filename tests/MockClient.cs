﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using uMod.Common;
using uMod.Common.WebSockets;

namespace uMod.Tests
{
    abstract public class MockClient
    {
        private readonly Random rnd = new Random();
        protected static Process process;
        protected static string FileName = "uMod.WebSockets.dll";
        protected static string BinaryPath;
        protected static volatile bool ready;
        protected static ObjectStreamClient<WebSocketMessage, WebSocketMessage> client;
        protected static Queue<WebSocketMessage> messageQueue;
        protected static List<WebSocketMessage> lastMessages = new List<WebSocketMessage>();
        protected static int messagesReceived = 0;
        public static bool ProcessExited = false;
        public static string ErrorMessage = string.Empty;
        public static string BaseDirectory = string.Empty;

        public static void CreateClient()
        {
            ProcessExited = false;
            messageQueue = new Queue<WebSocketMessage>();

            BinaryPath = Path.Combine(BaseDirectory, FileName);
            if (!File.Exists(BinaryPath))
            {
                Assert.Fail($"{FileName} not found {BinaryPath}");
            }

            string[] args = new string[0];
            /*
            string[] args = new string[1] {
                $"--logPath=\"{AppContext.BaseDirectory}\"",
            };
            */

            try
            {
                process = new Process
                {
                    StartInfo =
                    {
                        FileName = "dotnet",
                        Arguments =  $"{BinaryPath} {string.Join(" ", args)}",
                        WorkingDirectory = AppContext.BaseDirectory,
                        CreateNoWindow = true,
                        UseShellExecute = false,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                    },
                    EnableRaisingEvents = true,
                };
                process.Exited += OnExit;
                process.Start();
            }
            catch (Exception)
            {
                throw;
            }

            client = new ObjectStreamClient<WebSocketMessage, WebSocketMessage>(process.StandardOutput.BaseStream, process.StandardInput.BaseStream);

            client.Message += OnMessage;
            client.Error += OnError;
            client.Start();
            Console.WriteLine("Test client started");
        }

        public static void OnExit(object sender, EventArgs e)
        {
            Console.WriteLine($"Exited: {process.ExitCode}");
            ProcessExited = true;
        }

        public static void CloseClient()
        {
            process.Kill();
        }

        private static void OnMessage(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> connection, WebSocketMessage message)
        {
            if (message == null)
            {
                Console.WriteLine("null message");
                return;
            }

            Console.WriteLine($"Message: {message.Type}");

            switch (message.Type)
            {
                case WebSocketMessageType.Ready:
                    connection.PushMessage(message);
                    if (!ready)
                    {
                        ready = true;
                        while (messageQueue.Count > 0)
                        {
                            connection.PushMessage(messageQueue.Dequeue());
                        }
                    }
                    break;

                default:
                    lastMessages.Add(message);
                    messagesReceived++;

                    break;
            }
        }

        private static void OnError(Exception exception)
        {
            ErrorMessage = exception.Message;
        }

        protected WebSocketMessage CreateMessage(WebSocketMessageType messageType, object data, object extraData = null)
        {
            return new WebSocketMessage()
            {
                Id = rnd.Next(1, 999999),
                Type = messageType,
                Data = data,
                ExtraData = extraData
            };
        }

        protected void EnqueueMessage(WebSocketMessage message)
        {
            messagesReceived = 0;
            lastMessages = new List<WebSocketMessage>();

            if (ready)
            {
                client.PushMessage(message);
            }
            else
            {
                messageQueue.Enqueue(message);
            }

            int retries = 0;
            while (messagesReceived == 0)
            {
                if (retries > 25)
                {
                    break;
                }
                Thread.Sleep(250);
                retries++;
            };
        }
    }
}
