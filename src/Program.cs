using Mono.Options;
using System.IO;

namespace uMod.WebSockets
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string logPath = null;
            string dataPath = null;

            OptionSet optionSet = new OptionSet
            {
                { "l|logPath=", "the database logging directory.",  value => { if (Directory.Exists(value)) { logPath = value; } }},
                { "d|dataPath=", "the database data directory.",  value => { if (Directory.Exists(value)) { dataPath = value; } }},
            };

            try
            {
                optionSet.Parse(args);
            }
            catch (OptionException)
            {
            }

            SocketService client = new SocketService(logPath, dataPath);

            client.Listen();
        }
    }
}
