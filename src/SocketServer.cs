﻿using Fleck;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using uMod.Common;
using uMod.Common.WebSockets;
using IWebSocketConnection = Fleck.IWebSocketConnection;

namespace uMod.WebSockets
{
    public class SocketServer
    {
        public ConnectionInfo Info { get; }
        public WebSocketServer Server { get; private set; }
        public ConnectionState State { get; internal set; }
        private readonly SocketService _socketService;
        private readonly ILogger _logger;
        internal readonly Dictionary<string, Common.IWebSocketConnection> Connections = new Dictionary<string, Common.IWebSocketConnection>();

        public int ConnectionCount => Connections.Count;

        /// <summary>
        /// Create new instance of the SocketServer class
        /// </summary>
        /// <param name="service"></param>
        /// <param name="info"></param>
        public SocketServer(SocketService service, ConnectionInfo info, ILogger logger)
        {
            Info = info;
            if (Info.Path == null)
            {
                Info.Path = "/";
            }
            _logger = logger;
            _socketService = service;
            string address = $"{info.Protocol}://{info.Location}:{info.Port}";
            Server = new WebSocketServer(address, info.SupportDualStack);
        }

        /// <summary>
        /// Gets a connection with the specified identifier
        /// </summary>
        /// <param name="id"></param>
        /// <param name="conn"></param>
        /// <returns></returns>
        public bool TryGetConnection(string id, out Common.IWebSocketConnection conn)
        {
            return Connections.TryGetValue(id, out conn);
        }

        /// <summary>
        /// Gets all connections
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Common.IWebSocketConnection> GetConnections()
        {
            return Connections.Select(x => x.Value);
        }

        /// <summary>
        /// Start server
        /// </summary>
        public void Start()
        {
            Server.Start(OnConnected);
            State = ConnectionState.Open;
        }

        /// <summary>
        /// Server connected
        /// </summary>
        /// <param name="socket"></param>
        public void OnConnected(IWebSocketConnection socket)
        {
            socket.OnOpen = () => OnOpen(socket);
            socket.OnClose = () => OnClose(socket);
            socket.OnMessage = message => OnMessage(socket, message);
        }

        /// <summary>
        /// Socket sent message
        /// </summary>
        /// <param name="socket"></param>
        /// <param name="message"></param>
        private void OnMessage(IWebSocketConnection socket, string message)
        {
            _socketService.Server.PushMessage(new WebSocketMessage()
            {
                ClientId = socket.ConnectionInfo.Id.ToString(),
                Type = WebSocketMessageType.SocketPacket,
                Data = JsonConvert.DeserializeObject<Packet>(message),
                ExtraData = Info.Name
            });
        }

        /// <summary>
        /// Socket closed connection
        /// </summary>
        /// <param name="socket"></param>
        private void OnClose(IWebSocketConnection socket)
        {
            string id = socket.ConnectionInfo.Id.ToString();
            Connections.Remove(id);
            _logger.Info($"Disconnected: {id}");
            _socketService.Server.PushMessage(new WebSocketMessage()
            {
                ClientId = id,
                Type = WebSocketMessageType.SocketClose,
            });
        }

        /// <summary>
        /// Socket opened connection
        /// </summary>
        /// <param name="socket"></param>
        private void OnOpen(IWebSocketConnection socket)
        {
            if (!socket?.ConnectionInfo?.Path?.TrimStart('/').Equals(Info?.Path?.TrimStart('/'), System.StringComparison.InvariantCultureIgnoreCase) ?? true)
            {
                socket.Close();
            }
            string id = socket.ConnectionInfo.Id.ToString();
            SocketConnection socketConnection = new SocketConnection()
            {
                ConnectionName = Info.Name,
                FleckConnection = socket
            };
            Connections.Add(id, socketConnection);
            _logger.Info($"Connected: {socketConnection.Address} ({id})");
            _socketService.Server.PushMessage(new WebSocketMessage()
            {
                ClientId = id,
                Type = WebSocketMessageType.SocketStart,
                Data = socketConnection
            });
        }

        public void Close()
        {
            _logger?.Info($"Closing server {Info.Name}");
            Server?.ListenerSocket?.Close();
            State = ConnectionState.Closed;
        }
    }
}
