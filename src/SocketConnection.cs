﻿using Newtonsoft.Json;
using System;

namespace uMod.WebSockets
{
    [Serializable]
    public class SocketConnection : Common.WebSockets.WebSocketConnection
    {
        public override string Id => FleckConnection.ConnectionInfo.Id.ToString();

        public override string Address => FleckConnection?.ConnectionInfo.ClientIpAddress;

        public override int Port => FleckConnection.ConnectionInfo.ClientPort;

        public override Common.WebSocketConnectionState State => FleckConnection.IsAvailable ? Common.WebSocketConnectionState.Open : Common.WebSocketConnectionState.Closed;

        [NonSerialized]
        public Fleck.IWebSocketConnection FleckConnection;

        public new string ConnectionName { get; set; }

        public SocketConnection()
        {
        }

        public override void Close(int code = 1000)
        {
            FleckConnection?.Close(code);
        }

        public override void Send(string data)
        {
            FleckConnection?.Send(data);
        }

        public override void Send(byte[] data)
        {
            FleckConnection?.Send(data);
        }

        public override void Send(object data)
        {
            Send(JsonConvert.SerializeObject(data));
        }

        public override void Send<T>(T data)
        {
            Send(JsonConvert.SerializeObject(data));
        }
    }
}
