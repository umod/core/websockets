using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using uMod.Common;
using uMod.Common.Logging;
using uMod.Common.WebSockets;

namespace uMod.WebSockets
{
    /// <summary>
    /// Database service
    /// </summary>
    public class SocketService
    {
        private bool _shouldRun = true;
        private bool _ready;
        private bool _exitReceived;
        private readonly string _logPath;
        private readonly string _dataPath;
        private ILogger _logger;
        private readonly Dictionary<string, ConnectionInfo> _connectionInfos = new Dictionary<string, ConnectionInfo>();
        private readonly Dictionary<string, SocketServer> _servers = new Dictionary<string, SocketServer>();

        public ObjectStreamClient<WebSocketMessage, WebSocketMessage> Server;

        /// <summary>
        /// Create a new instance of the DatabaseService class
        /// </summary>
        /// <param name="logPath"></param>
        /// <param name="dataPath"></param>
        public SocketService(string logPath = null, string dataPath = null)
        {
            //System.Diagnostics.Debugger.Launch();
            _logPath = logPath;
            if (string.IsNullOrEmpty(logPath))
            {
                _logPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            }
            _dataPath = dataPath ?? Environment.CurrentDirectory;
        }

        public SocketServer GetServer(string name)
        {
            if (_servers.TryGetValue(name, out SocketServer server))
            {
                return server;
            }

            return null;
        }

        /// <summary>
        /// Gets the name of the log file
        /// </summary>
        /// <returns></returns>
        private string GetLogFileName()
        {
            return $"websockets_{DateTime.UtcNow:yyyy-MM-dd}.log";
        }

        public ILogger CreateDebugLogger(string logPath = null)
        {
            logPath ??= Environment.CurrentDirectory;
            if (!Directory.Exists(logPath))
            {
                Directory.CreateDirectory(logPath);
            }
            string logFilePath = Path.Combine(logPath, GetLogFileName());

            return _logger = new DebugFileLogger(logFilePath, "WebSockets");
        }

        /// <summary>
        /// Begin service listener
        /// </summary>
        public void Listen(Stream inputStream = null, Stream outputStream = null)
        {
            CreateDebugLogger(_logPath);
            inputStream ??= Console.OpenStandardInput();
            outputStream ??= Console.OpenStandardOutput();
            try
            {
                Server = new ObjectStreamClient<WebSocketMessage>(inputStream, outputStream, null, _logger);
                Server.Message += OnMessage;
                Server.Error += OnError;
                Server.Start();
                int tries = 0;
                while (!_exitReceived && (_shouldRun || _ready))
                {
                    _shouldRun = !_ready && tries++ < 60; // Wait 30 seconds for bootup
                    if (!_ready)
                    {
                        Server.PushMessage(new WebSocketMessage { Type = WebSocketMessageType.Ready });
                    }

                    Thread.Sleep(1000);
                }

                Server.Stop();
            }
            catch (Exception ex)
            {
                OnError(ex);
            }
            finally
            {
                Server = null;
            }
        }

        /// <summary>
        /// Handle service messages
        /// </summary>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        public void OnMessage(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            if (message == null)
            {
                _logger?.Info("Connection closed.");
                _exitReceived = true;
                return;
            }

            _logger?.Info($"Received WebSocketMessage: {message.Type}: {message.Id}");
            switch (message.Type)
            {
                case WebSocketMessageType.SocketPacket:
                    Send(objectStream, message);
                    break;

                case WebSocketMessageType.SocketStatus:
                    Handle(Status, objectStream, message);
                    break;

                case WebSocketMessageType.ServerStart:
                    Handle(HandleServerStart, objectStream, message);
                    break;

                case WebSocketMessageType.SocketClose:
                case WebSocketMessageType.ServerClose:
                    Handle(HandleClose, objectStream, message);
                    break;

                case WebSocketMessageType.Exit:
                    Disconnect(_servers);
                    _exitReceived = true;
                    break;

                case WebSocketMessageType.Ready:
                    _ready = true;
                    break;
            }
        }

        /// <summary>
        /// Provide message to specified message handle and push a response
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        public void Handle(Func<ObjectStreamConnection<WebSocketMessage, WebSocketMessage>, WebSocketMessage, WebSocketMessage> callback, ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            WebSocketMessage response = TryHandle(callback, objectStream, message);
            if (response != null)
            {
                _logger?.Info($"Replying {response.Type} ({response.Id})");
                objectStream.PushMessage(response);
            }
        }

        public WebSocketMessage TryHandle(Func<ObjectStreamConnection<WebSocketMessage, WebSocketMessage>, WebSocketMessage, WebSocketMessage> callback, ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            try
            {
                return callback.Invoke(objectStream, message);
            }
            catch (Exception ex)
            {
                return new WebSocketMessage { Id = message.Id, Data = $"General exception: {ex.Message}", Type = WebSocketMessageType.Error };
            }
        }

        /// <summary>
        /// Handle status message
        /// </summary>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public WebSocketMessage Status(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            if (message.Data is not string connectionName)
            {
                return new WebSocketMessage { Id = message.Id, Data = "Connection info does not contain a valid connection name", ExtraData = ConnectionState.Closed, Type = WebSocketMessageType.Error };
            }

            if (_servers.TryGetValue(connectionName, out SocketServer server))
            {
                if (message.ExtraData is string connectionId)
                {
                    if (server.Connections.TryGetValue(connectionId, out IWebSocketConnection connection))
                    {
                        return new WebSocketMessage { Id = message.Id, Data = connection.State, Type = WebSocketMessageType.SocketStatus };
                    }
                    else
                    {
                        return new WebSocketMessage { Id = message.Id, Data = WebSocketConnectionState.Closed, Type = WebSocketMessageType.SocketStatus };
                    }
                }

                return new WebSocketMessage { Id = message.Id, Data = server.State, Type = WebSocketMessageType.ServerStatus };
            }
            else
            {
                if (message.ExtraData is string)
                {
                    return new WebSocketMessage { Id = message.Id, Data = WebSocketConnectionState.Closed, Type = WebSocketMessageType.SocketStatus };
                }

                return new WebSocketMessage { Id = message.Id, Data = ConnectionState.Closed, Type = WebSocketMessageType.ServerStatus };
            }
        }

        /// <summary>
        /// Handle connect message
        /// </summary>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public WebSocketMessage HandleServerStart(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            if (message.Data is not ConnectionInfo info)
            {
                return new WebSocketMessage { Id = message.Id, Data = "Connection info does not contain valid connection", ExtraData = ConnectionState.Closed, Type = WebSocketMessageType.Error };
            }

            if (!_connectionInfos.ContainsKey(info.Name))
            {
                _connectionInfos.Add(info.Name, info);
            }
            else
            {
                _connectionInfos[info.Name] = info;
            }

            if (_servers.TryGetValue(info.Name, out SocketServer server))
            {
                Disconnect(server);
                _servers.Remove(info.Name);
            }

            _logger?.Info($"Starting {info}...");
            ConnectionState connectionState = StartServer(info, out _, out string responseMessage);

            _logger?.Info(responseMessage);
            return new WebSocketMessage { Id = message.Id, Data = connectionState, ExtraData = responseMessage, Type = WebSocketMessageType.ServerStatus };
        }

        /// <summary>
        /// Handle disconnect message
        /// </summary>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public WebSocketMessage HandleClose(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            if (message.Data is not string serverName)
            {
                return new WebSocketMessage { Id = message.Id, Data = "Connection info does not contain a valid connection name", ExtraData = ConnectionState.Closed, Type = WebSocketMessageType.Error };
            }

            if (_servers.TryGetValue(serverName, out SocketServer server))
            {
                if (message.ExtraData is string connectionId)
                {
                    int closeCode = 1000;
                    if (message.ExtraData2 is int)
                    {
                        closeCode = (int)message.ExtraData2;
                    }
                    if (server.Connections.TryGetValue(connectionId, out IWebSocketConnection conn))
                    {
                        conn?.Close(closeCode);
                    }

                    return new WebSocketMessage { Id = message.Id, Data = WebSocketConnectionState.Closed, ExtraData = connectionId, Type = WebSocketMessageType.SocketStatus };
                }
                else
                {
                    Disconnect(server);
                }
            }

            return new WebSocketMessage { Id = message.Id, Data = ConnectionState.Closed, ExtraData = serverName, Type = WebSocketMessageType.ServerStatus };
        }

        /// <summary>
        /// Connect to database using specified connection info
        /// </summary>
        /// <param name="info"></param>
        /// <param name="server"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private ConnectionState StartServer(ConnectionInfo info, out SocketServer server, out string message)
        {
            if (!_servers.TryGetValue(info.Name, out server))
            {
                server = CreateServer(info);
                if (server != null)
                {
                    _servers.Add(info.Name, server);
                }
                else
                {
                    message = $"Server {info.Name} did not start";
                    return ConnectionState.Closed;
                }
            }

            if (server.State == ConnectionState.Open)
            {
                message = $"Server {info.Name} already started";
                return server.State;
            }

            try
            {
                server.Start();
                if (server.State == ConnectionState.Open)
                {
                    message = $"Server {info.Name} started";
                    return server.State;
                }
            }
            catch (Exception ex)
            {
                message = $"Server {info.Name} failed with error: {ex.Message}{Environment.NewLine}{ex.StackTrace}";
                return server.State;
            }

            message = $"Server {info.Name} did not start";
            return server.State;
        }

        /// <summary>
        /// Disconnect and close specified socket server
        /// </summary>
        /// <param name="info"></param>
        /// <param name="connection"></param>
        /// <param name="dispose"></param>
        private void Disconnect(SocketServer server)
        {
            if (server == null) return;
            _logger?.Info($"Closing server: {server.Info.Name}");
            try
            {
                server.Close();
            }
            catch (Exception ex)
            {
                _logger?.Error($"Disconnect failure: {ex.Message}");
            }
        }

        /// <summary>
        /// Disconnect from all specified database connections
        /// </summary>
        /// <param name="connections"></param>
        private void Disconnect(IDictionary<string, SocketServer> connections)
        {
            foreach (KeyValuePair<string, SocketServer> kvp in connections)
            {
                if (!_connectionInfos.TryGetValue(kvp.Key, out _))
                {
                    continue;
                }

                Disconnect(kvp.Value);
            }
        }

        /// <summary>
        /// Handle query message
        /// </summary>
        /// <param name="objectStream"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public void Send(ObjectStreamConnection<WebSocketMessage, WebSocketMessage> objectStream, WebSocketMessage message)
        {
            try
            {
                if (message.ExtraData is not string connectionName)
                {
                    throw new InvalidOperationException();
                }

                if (_servers.TryGetValue(connectionName, out SocketServer server) &&
                    !string.IsNullOrEmpty(message.ClientId) &&
                    server.Connections.TryGetValue(message.ClientId, out IWebSocketConnection conn))
                {
                    if (message.Data is byte[] bytes)
                    {
                        conn.Send(bytes);
                    }
                    else if (message.Data is string data)
                    {
                        conn.Send(data);
                    }
                    else
                    {
                        conn.Send(message.Data);
                    }
                }
            }
            catch (Exception e)
            {
                _logger?.Report("Packet failed to send", e);
            }
        }

        /// <summary>
        /// Create a new database connection using the specified connection info
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <returns></returns>
        private SocketServer CreateServer(ConnectionInfo connectionInfo)
        {
            return new SocketServer(this, connectionInfo, _logger);
        }

        /// <summary>
        /// Log errors
        /// </summary>
        /// <param name="exception"></param>
        public void OnError(Exception exception)
        {
            _logger?.Report(exception);
        }
    }
}
